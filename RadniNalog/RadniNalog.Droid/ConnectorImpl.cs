using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using RadniNalog.Droid;
using RadniNalog.Native;
using SQLite;
using System.IO;
using SQLite.Net;
using System.Net;
using SQLite.Net.Platform.XamarinAndroid;

[assembly: Xamarin.Forms.Dependency(typeof(ConnectorImpl))]
namespace RadniNalog.Droid
{
    class ConnectorImpl : IConnector
    {
        public SQLiteConnection GetConnection()
        {
            String sqliteFilename = "MySQLite.db3";
            String documentsPath = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
            String path = Path.Combine(documentsPath, sqliteFilename);
            SQLitePlatformAndroid sqlitePlatformAndroid = new SQLitePlatformAndroid();
            return new SQLiteConnection(sqlitePlatformAndroid, path);
        }
    }
}