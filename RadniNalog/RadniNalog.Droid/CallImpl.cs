using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;

using RadniNalog.Droid;
using RadniNalog.Native;

[assembly: Xamarin.Forms.Dependency (typeof(CallImpl))]
namespace RadniNalog.Droid
{
    public class CallImpl : ICall
    {
        public void CallSomeone(string number)
        {
            var uri = Android.Net.Uri.Parse("tel:" + number);//"tel:1112223333"
            var intent = new Intent(Intent.ActionDial, uri);
            Forms.Context.StartActivity(intent);
            
        }

        //[Android.Runtime.Register("startActivity", "(Landroid/content/Intent;)V", "GetStartActivity_Landroid_content_Intent_Handler")]
        //public abstract Void StartActivity(Intent intent);
    }
}