﻿using RadniNalog.Model;
using RadniNalog.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace RadniNalog
{
    public partial class WorkUnitPage : ContentPage
    {
        private ObservableCollection<WorkUnit> workUnitCollection = null;
        private SQLiteRepositoryImpl sqliteRepository = null;

        public WorkUnitPage(SQLiteRepositoryImpl sqliteRepository)
        {
            InitializeComponent();
            this.sqliteRepository = sqliteRepository;
            workUnitCollection = new ObservableCollection<WorkUnit>();
            ListViewWorkUnits.ItemsSource = workUnitCollection;
        }

        protected override void OnAppearing()
        {
            workUnitCollection.Clear();
            List<WorkUnit> workUnitList = sqliteRepository.GetAllEntities<WorkUnit>();
            foreach(WorkUnit workUnit in workUnitList)
            {
                workUnitCollection.Add(workUnit);
            }
        }

        void btn_AddWorkUnit(object sender, EventArgs args)
        {
            NavigateTo(new AddWorkUnitPage(sqliteRepository));
        }

        void menuItem_Edit(object sender, EventArgs args)
        {
            WorkUnit workUnit = (WorkUnit)ListViewWorkUnits.SelectedItem;
        }

        void menuItem_Delete(object sender, EventArgs args)
        {
            var item = (MenuItem)sender;
            WorkUnit listitem = (from itm in workUnitCollection
                                 where itm.ID.ToString() == item.CommandParameter.ToString()
                             select itm).FirstOrDefault<WorkUnit>();
            workUnitCollection.Remove(listitem);
            //WorkUnit workUnit = (WorkUnit)(item).BindingContext;
            sqliteRepository.DeleteEntity<WorkUnit>(listitem);
        }

        async private void NavigateTo(Page page)
        {
            await Navigation.PushAsync(page);
        }
    }
}
