﻿using RadniNalog.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace RadniNalog.ViewModel
{
    public partial class AddWorkUnitPage : ContentPage
    {
        private WorkUnit workUnit = null;
        private SQLiteRepositoryImpl sqliteRepository = null;

        public AddWorkUnitPage(SQLiteRepositoryImpl sqliteRepository)
        {
            InitializeComponent();
            this.sqliteRepository = sqliteRepository;
            this.workUnit = new WorkUnit();
            this.BindingContext = workUnit;
        }

        void btn_AddWorkingUnit(object sender, EventArgs args)
        {
            if (!String.IsNullOrEmpty(workUnit.WorkUnitDescription))
            {
                sqliteRepository.SaveEntity<WorkUnit>(workUnit);
                Navigation.PopAsync();
            }
            ShowMessage(MessageType.ALERT, "Work unit description cannot be empty", "OK");
        }

        private async void ShowMessage(string title, string message, string button)
        {
            await DisplayAlert(title, message, button);
        }
    }
}
