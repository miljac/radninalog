﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RadniNalog.Model;

using Xamarin.Forms;

namespace RadniNalog.ViewModel
{
    public partial class ViewWorkOrderPage : ContentPage
    {
        private WorkOrder workOrder = null;
        public ViewWorkOrderPage(WorkOrder workOrder)
        {
            InitializeComponent();
            this.workOrder = workOrder;
            this.BindingContext = this.workOrder;
        }
    }
}
