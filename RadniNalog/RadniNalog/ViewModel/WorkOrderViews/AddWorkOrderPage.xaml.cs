﻿using RadniNalog.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace RadniNalog.ViewModel
{
    public partial class AddWorkOrderPage : ContentPage
    {
        private WorkOrder workOrder = null;
        private SQLiteRepositoryImpl sqliteRepository = null;
        private List<WorkUnit> workUnitList = null;
        private List<Organization> organizationList = null;
        private ObservableCollection<WorkDone> workDoneCollection = null;
        private readonly String CANCEL = "Cancel";
        private readonly String OK = "OK";

        public AddWorkOrderPage(SQLiteRepositoryImpl sqliteRepository)
        {
            InitializeComponent();
            this.sqliteRepository = sqliteRepository;
            this.workOrder = new WorkOrder(Guid.NewGuid());
            workOrder.DateOpened = DateTime.Now;
            workOrder.DateStarted = DateTime.Now;
            workOrder.DateEnded = DateTime.Now;
            workUnitList = sqliteRepository.GetAllEntities<WorkUnit>();
            organizationList = sqliteRepository.GetAllEntities<Organization>();
            workDoneCollection = new ObservableCollection<WorkDone>();
            this.BindingContext = workOrder;
            ListViewWorkDone.ItemsSource = workDoneCollection;
        }

        void btn_AddWorkOrder(object sender, EventArgs args)
        {
            if (workOrder.Organization != null)
            {
                workOrder.OrganizationId = workOrder.Organization.ID;
                workOrder.workDoneList = new List<WorkDone>(workDoneCollection);
                sqliteRepository.SaveEntity<WorkOrder>(workOrder);
                foreach (WorkDone workDone in workDoneCollection)
                {
                    workDone.WorkOrderId = workOrder.ID;
                    sqliteRepository.SaveEntity<WorkDone>(workDone);
                }
                Navigation.PopAsync();
            }
            else
            {
                ShowMessage(MessageType.ALERT, "Organization must be specified", OK);          
            }
        }

        private async void ShowMessage(string title, string message, string button)
        {
            await DisplayAlert(title, message, button);
        }

        async void NavigateTo(Page page)
        {
            await Navigation.PushModalAsync(page);
        }

        void btn_plus1(object sender, EventArgs e)
        {
            var item = (Button)sender;
            WorkDone listitem = (from itm in workDoneCollection
                                 where itm.Guid == item.CommandParameter.ToString()
                                 select itm).FirstOrDefault<WorkDone>();
            listitem.Hours += 1;
            RefreshListView();
        }

        void btn_minus1(object sender, EventArgs e)
        {
            var item = (Button)sender;
            WorkDone listitem = (from itm in workDoneCollection
                                 where itm.Guid == item.CommandParameter.ToString()
                                 select itm).FirstOrDefault<WorkDone>();
            listitem.Hours -= 1;
            RefreshListView();
        }

        private void RefreshListView()
        {
            ListViewWorkDone.ItemsSource = null;
            ListViewWorkDone.ItemsSource = workDoneCollection;
        }

        async void btn_SelectWorkUnits(object sender, EventArgs e)
        {
            String[] workUnitDescriptions = workUnitList.Select(x => x.WorkUnitDescription).ToArray();
            String action = await DisplayActionSheet("Select work:", CANCEL, null, workUnitDescriptions);
            if (action != CANCEL)
            {
                WorkUnit selectedWorkUnit = workUnitList.Where(x => x.WorkUnitDescription == action).First();
                if (selectedWorkUnit != null)
                {
                    WorkDone workDone = new WorkDone(Guid.NewGuid());
                    workDone.WorkUnit = selectedWorkUnit;
                    workDone.WorkUnitId = selectedWorkUnit.ID;
                    workDoneCollection.Add(workDone);
                }
            }
        }

        async void btn_SelectOrganization(object sender, EventArgs e)
        {
            String[] organizationNames = organizationList.Select(x => x.Name).ToArray();
            String action = await DisplayActionSheet("Select organization:", CANCEL, null, organizationNames);
            if (action != CANCEL)
            {
                workOrder.Organization = organizationList.Where(x => x.Name == action).First();
                lblOrganizationName.Text = workOrder.Organization.Name;
            }
        }

        void menuItem_Delete(object sender, EventArgs args)
        {
            var item = (MenuItem)sender;
            WorkDone listitem = (from itm in workDoneCollection
                                     where itm.Guid == item.CommandParameter.ToString()
                                     select itm).FirstOrDefault<WorkDone>();
            workDoneCollection.Remove(listitem);
        }

        void menuItem_plus10(object sender, EventArgs args)
        {
            var item = (MenuItem)sender;
            WorkDone listitem = (from itm in workDoneCollection
                                 where itm.Guid == item.CommandParameter.ToString()
                                 select itm).FirstOrDefault<WorkDone>();
            listitem.Hours += 10;
            RefreshListView();
        }

        void menuItem_minus10(object sender, EventArgs args)
        {
            var item = (MenuItem)sender;
            WorkDone listitem = (from itm in workDoneCollection
                                 where itm.Guid == item.CommandParameter.ToString()
                                 select itm).FirstOrDefault<WorkDone>();
            if (listitem.Hours > 9) listitem.Hours -= 10;
            RefreshListView();
        }

        void menuItem_plus5(object sender, EventArgs args)
        {
            var item = (MenuItem)sender;
            WorkDone listitem = (from itm in workDoneCollection
                                 where itm.Guid == item.CommandParameter.ToString()
                                 select itm).FirstOrDefault<WorkDone>();
            listitem.Hours += 5;
            RefreshListView();
        }

        void menuItem_minus5(object sender, EventArgs args)
        {
            var item = (MenuItem)sender;
            WorkDone listitem = (from itm in workDoneCollection
                                 where itm.Guid == item.CommandParameter.ToString()
                                 select itm).FirstOrDefault<WorkDone>();
            if (listitem.Hours > 4) listitem.Hours -= 5;
            RefreshListView();
        }

        void listView_WorkDone(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem == null)
            {
                return;
            }
            ((ListView)sender).SelectedItem = null;
        }
    }
}
