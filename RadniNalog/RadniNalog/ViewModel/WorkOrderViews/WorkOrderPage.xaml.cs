﻿using Newtonsoft.Json;
using RadniNalog.Model;
using RadniNalog.Service;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace RadniNalog.ViewModel
{
    public partial class WorkOrderPage : ContentPage
    {
        private ObservableCollection<WorkOrder> workOrderCollection = null;
        private SQLiteRepositoryImpl sqliteRepository = null;
        private RestService restService = null;

        public WorkOrderPage(SQLiteRepositoryImpl sqliteRepository)
        {
            InitializeComponent();
            this.sqliteRepository = sqliteRepository;
            workOrderCollection = new ObservableCollection<WorkOrder>();
            restService = new RestService();
            ListViewWorkOrders.ItemsSource = workOrderCollection;
        }

        protected override void OnAppearing()
        {
            workOrderCollection.Clear();
            List<WorkOrder> workOrderList = sqliteRepository.GetAllEntities<WorkOrder>();
            foreach (WorkOrder workOrder in workOrderList)
            {
                sqliteRepository.GetChildern<WorkOrder>(workOrder, false);
                foreach(WorkDone workDone in workOrder.workDoneList)
                {
                    sqliteRepository.GetChildern<WorkDone>(workDone, false);
                }
                if (workOrder.Organization != null)
                {
                    sqliteRepository.GetChildern<Organization>(workOrder.Organization, false);
                    sqliteRepository.GetChildern<Address>(workOrder.Organization.Address, false);
                }
                workOrderCollection.Add(workOrder);
            }
        }

        void menuItem_POST(object sender, EventArgs args)
        {
            var item = (MenuItem)sender;
            WorkOrder listitem = (from itm in workOrderCollection
                                     where itm.ID.ToString() == item.CommandParameter.ToString()
                                     select itm).FirstOrDefault<WorkOrder>();
            restService.PostToServer<WorkOrder>(listitem);
        }

        void menuItem_Delete(object sender, EventArgs args)
        {
            var item = (MenuItem)sender;
            WorkOrder listitem = (from itm in workOrderCollection
                                  where itm.ID.ToString() == item.CommandParameter.ToString()
                                  select itm).FirstOrDefault<WorkOrder>();
            workOrderCollection.Remove(listitem);
            sqliteRepository.DeleteEntity<WorkOrder>(listitem);
        }

        void menuItem_ViewOrder(object sender, EventArgs args)
        {
            var item = (MenuItem)sender;
            WorkOrder listitem = (from itm in workOrderCollection
                                  where itm.ID.ToString() == item.CommandParameter.ToString()
                                  select itm).FirstOrDefault<WorkOrder>();
            NavigateTo(new ViewWorkOrderPage(listitem));
        }

        void btn_AddWorkOrder(object sender, EventArgs args)
        {
            NavigateTo(new AddWorkOrderPage(sqliteRepository));
        }

        async void NavigateTo(Page page)
        {
            await Navigation.PushAsync(page);
        }
    }
}
