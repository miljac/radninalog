﻿using RadniNalog.Model;
using RadniNalog.Service;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace RadniNalog.ViewModel.WorkOrderViews
{
    public partial class ViewWorkOrderServer : ContentPage
    {
        private ObservableCollection<WorkOrder> workOrderCollection = null;
        private SQLiteRepositoryImpl sqliteRepository = null;
        private RestService restService = null;

        public ViewWorkOrderServer(SQLiteRepositoryImpl sqliteRepository)
        {
            InitializeComponent();
            this.sqliteRepository = sqliteRepository;
            workOrderCollection = new ObservableCollection<WorkOrder>();
            restService = new RestService();
            ListViewWorkOrders.ItemsSource = workOrderCollection;
        }

        protected override void OnAppearing()
        {
            Sync();
        }

        void menuItem_ViewOrder(object sender, EventArgs args)
        {
            var item = (MenuItem)sender;
            WorkOrder listitem = (from itm in workOrderCollection
                                  where itm.ID.ToString() == item.CommandParameter.ToString()
                                  select itm).FirstOrDefault<WorkOrder>();
            NavigateTo(new ViewWorkOrderPage(listitem));
        }

        void btn_Sync(object sender, EventArgs args)
        {
            Sync();
        }

        async void NavigateTo(Page page)
        {
            await Navigation.PushAsync(page);
        }

        async void Sync()
        {
            List<WorkOrder> serverWorkOrderList = await restService.GetAllFromServer<WorkOrder>();
            workOrderCollection.Clear();
            foreach (WorkOrder workOreder in serverWorkOrderList)
            {
                workOrderCollection.Add(workOreder);
            }
        }
    }
}
