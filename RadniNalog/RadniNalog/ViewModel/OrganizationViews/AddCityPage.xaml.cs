﻿using RadniNalog.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace RadniNalog.ViewModel
{
    public partial class AddCityPage : ContentPage
    {
        private City city = null;
        private SQLiteRepositoryImpl sqliteRepository = null;

        public AddCityPage(SQLiteRepositoryImpl sqliteRepository)
        {
            InitializeComponent();
            this.sqliteRepository = sqliteRepository;
            this.city = new City();
            this.BindingContext = city;
        }

        void btn_AddCity(object sender, EventArgs args)
        {
            if (!String.IsNullOrEmpty(city.Name))
            {
                sqliteRepository.SaveEntity<City>(city);
                Navigation.PopAsync();
            }
            else
            {
                ShowMessage(MessageType.ALERT, "City name cannot be empty", "OK");
            }
        }

        private async void ShowMessage(string title, string message, string button)
        {
            await DisplayAlert(title, message, button);
        }
    }
}
