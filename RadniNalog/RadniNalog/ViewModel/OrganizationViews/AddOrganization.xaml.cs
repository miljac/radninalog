﻿using RadniNalog.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace RadniNalog.ViewModel
{
    public partial class AddOrganization : ContentPage
    {
        List<City> cityList = null;
        Organization organization = null;
        Address address = null;
        private SQLiteRepositoryImpl sqliteRepository = null;

        public AddOrganization(SQLiteRepositoryImpl sqliteRepository)
        {
            InitializeComponent();
            this.sqliteRepository = sqliteRepository;
            this.organization = new Organization();
            this.address = new Address();
            cityList = new List<City>();
            organization.Address = address;
            this.BindingContext = organization;
        }

        protected override void OnAppearing()
        {
            cityList = sqliteRepository.GetAllEntities<City>();
            PickerCity.Items.Clear();
            foreach(City city in cityList)
            {
                PickerCity.Items.Add(city.Name);
            }
            if (PickerCity.Items.Count > 0) PickerCity.SelectedIndex = 0;
        }

        void btn_AddCity(object sender, EventArgs args)
        {
            NavigateTo(new AddCityPage(sqliteRepository));
        }

        void btn_AddOrganization(object sender, EventArgs args)
        {
            if (String.IsNullOrEmpty(EntryName.Text) ||
                    String.IsNullOrEmpty(EntryStreetName.Text) ||
                    String.IsNullOrEmpty(EntryStreetNumber.Text))
            {
                ShowMessage(MessageType.ALERT, "Name, stret name and street number must be entered", "OK");
            }
            else
            {
                int selectedCity = PickerCity.SelectedIndex;
                String cityName = PickerCity.Items[selectedCity];
                foreach (City city in cityList)
                {
                    if (city.Name.Equals(cityName))
                    {
                        address.City = city;
                        address.CityId = city.ID;
                        break;
                    }
                }
                sqliteRepository.SaveEntity<Address>(address);
                organization.AddressId = address.ID;
                sqliteRepository.SaveEntity<Organization>(organization);
                Navigation.PopAsync();
            }
        }

        private async void ShowMessage(string title, string message, string button)
        {
            await DisplayAlert(title, message, button);
        }

        async void NavigateTo(Page page)
        {
            await Navigation.PushAsync(page);
        }
    }
}
