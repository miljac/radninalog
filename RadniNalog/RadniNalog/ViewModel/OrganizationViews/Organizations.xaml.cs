﻿using RadniNalog.Model;
using RadniNalog.Native;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace RadniNalog.ViewModel
{
    public partial class Organizations : ContentPage
    {
        private ObservableCollection<Organization> organizationCollection = null;
        private SQLiteRepositoryImpl sqliteRepository = null;

        public Organizations(SQLiteRepositoryImpl sqliteRepository)
        {
            InitializeComponent();
            this.sqliteRepository = sqliteRepository;
            organizationCollection = new ObservableCollection<Organization>();
            ListViewOrganizations.ItemsSource = organizationCollection;
        }

        protected override void OnAppearing()
        {
            organizationCollection.Clear();
            List<Organization> organizationList = sqliteRepository.GetAllEntities<Organization>();
            foreach (Organization organization in organizationList)
            {
                sqliteRepository.GetChildern<Organization>(organization, false);
                sqliteRepository.GetChildern<Address>(organization.Address, false);
                organizationCollection.Add(organization);
            }
        }

        void btn_AddOrganization(object sender, EventArgs args)
        {
            NavigateTo(new AddOrganization(sqliteRepository));
        }

        void menuItem_Delete(object sender, EventArgs args)
        {
            var item = (MenuItem)sender;
            Organization listitem = (from itm in organizationCollection
                                 where itm.ID.ToString() == item.CommandParameter.ToString()
                                 select itm).FirstOrDefault<Organization>();
            organizationCollection.Remove(listitem);
            sqliteRepository.DeleteEntity<Organization>(listitem);
        }

        void menuItem_Call(object sender, EventArgs args)
        {
            Organization org = (Organization)((MenuItem)sender).BindingContext;
            DependencyService.Get<ICall>().CallSomeone(org.PhoneNumber);
        }

        async void NavigateTo(Page page)
        {
            await Navigation.PushAsync(page);
        }
    }
}
