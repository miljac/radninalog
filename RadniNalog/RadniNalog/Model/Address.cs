﻿using Newtonsoft.Json;
using SQLite;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadniNalog.Model
{
    public class Address : IHasID
    {
        [PrimaryKey, AutoIncrement, JsonIgnore]
        public int ID { get; set; }
        [ForeignKey(typeof(City)), JsonIgnore]
        public int CityId { get; set; }
        [OneToOne]
        public City City{ get; set; }
        public int PostCode { get; set; }
        public String StreetName { get; set; }
        public String StreetNumber { get; set; }

        public Address()
        {

        }

        public int GetID()
        {
            return ID;
        }

        public override string ToString()
        {
            if (City != null)
            {
                return StreetName + " " + StreetNumber + ", " + PostCode + " " + City.Name;
            }
            return StreetName + " " + StreetNumber;
        }
    }
}
