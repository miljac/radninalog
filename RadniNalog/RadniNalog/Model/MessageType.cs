﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadniNalog.Model
{
    public static class MessageType
    {
        public const String ERROR = "Error";
        public const String WARNING = "Warning";
        public const String ALERT = "Alert";
    }
}
