﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadniNalog.Model
{
    public interface IHasID
    {
        int GetID();
    }
}
