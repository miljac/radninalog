﻿using RadniNalog.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadniNalog.Native
{
    interface IRepository
    {
        T GetEntity<T>(int id) where T : class, IHasID, new();
        List<T> GetAllEntities<T>() where T : class, IHasID, new();
        void GetChildern<T>(T entity, bool recursive) where T : class, IHasID, new();
        void DeleteEntity<T>(int id) where T : class, IHasID, new();
        void DeleteEntity<T>(T entity) where T : IHasID;
        void SaveEntity<T>(T entity) where T : IHasID;
        void UpdateEntity<T>(T entity) where T : IHasID;
    }
}
