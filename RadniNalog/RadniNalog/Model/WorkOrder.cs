﻿using Newtonsoft.Json;
using SQLite;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadniNalog.Model
{
    public class WorkOrder : IHasID
    {
        [PrimaryKey, AutoIncrement, JsonIgnore]
        public int ID { get; set; }
        public String _id { get; set; }
        [ForeignKey(typeof(Organization)), JsonIgnore]
        public int OrganizationId { get; set; }
        [OneToOne]
        public Organization Organization { get; set; }

        public DateTime DateOpened { get; set; }
        public DateTime DateStarted { get; set; }
        public DateTime DateEnded { get; set; }

        [OneToMany]
        public List<WorkDone> workDoneList { get; set; }

        public WorkOrder()
        {
            
        }

        public WorkOrder(Guid guid)
        {
            this._id = guid.ToString();
        }

        public int GetID()
        {
            return ID;
        }
    }
}
