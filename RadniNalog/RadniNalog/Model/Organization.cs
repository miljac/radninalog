﻿using Newtonsoft.Json;
using SQLite;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadniNalog.Model
{
    public class Organization : IHasID
    {
        [PrimaryKey, AutoIncrement, JsonIgnore]
        public int ID { get; set; }
        [ForeignKey(typeof(Address)), JsonIgnore]
        public int AddressId { get; set; }
        [OneToOne]
        public Address Address { get; set; }
        [Unique]
        public String Name { get; set; }
        public String PhoneNumber { get; set; }
        [Ignore]
        public String AddressString { get { return Address.ToString(); } }

        public Organization()
        {

        }

        public int GetID()
        {
            return ID;
        }
    }
}
