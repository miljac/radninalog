﻿using Newtonsoft.Json;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadniNalog.Model
{
    public class WorkDone : IHasID
    {
        [PrimaryKey, AutoIncrement, JsonIgnore]
        public int ID { get; set; }
        [ForeignKey(typeof(WorkUnit)), JsonIgnore]
        public int WorkUnitId { get; set; }
        [ForeignKey(typeof(WorkOrder)), JsonIgnore]
        public int WorkOrderId { get; set; }
        public int Hours { get; set; }
        [Ignore, JsonIgnore]
        public String Guid { get; set; }
        [OneToOne]
        public WorkUnit WorkUnit { get; set; }

        public WorkDone()
        {

        }

        public WorkDone(Guid Guid)
        {
            this.Guid = Guid.ToString();
        }

        public int GetID()
        {
            return ID;
        }

        public override string ToString()
        {
            if (WorkUnit != null)
            {
                return WorkUnit.WorkUnitDescription + ", Hours spent: " + Hours;
            }
            return base.ToString();
        }
    }
}
