﻿using Newtonsoft.Json;
using SQLite;
using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadniNalog.Model
{
    public class WorkUnit : IHasID
    {
        [PrimaryKey, AutoIncrement, JsonIgnore]
        public int ID { get; set; }
        [Unique]
        public String WorkUnitDescription { get; set; }

        public WorkUnit()
        {

        }

        public int GetID()
        {
            return ID;
        }
    }
}
