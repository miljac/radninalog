﻿using Newtonsoft.Json;
using SQLite;
using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadniNalog.Model
{
    public class City : IHasID
    {
        [PrimaryKey, AutoIncrement, JsonIgnore]
        public int ID { get; set; }
        public String Name { get; set; }

        public City()
        {

        }

        public int GetID()
        {
            return ID;
        }
    }
}
