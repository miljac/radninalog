﻿using RadniNalog.Native;
using SQLite.Net;
using SQLiteNetExtensions.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadniNalog.Model
{
    public class SQLiteRepositoryImpl : IRepository
    {
        private SQLiteConnection connection;

        public SQLiteRepositoryImpl(SQLiteConnection connection)
        {
            this.connection = connection;
        }

        public void DeleteEntity<T>(int id) where T : class, IHasID, new()
        {
            connection.Delete<T>(id);
        }

        public void DeleteEntity<T>(T entity) where T : IHasID
        {
            connection.Delete<T>(entity.GetID());
        }

        public List<T> GetAllEntities<T>() where T : class, IHasID, new()
        {
            return (from i in connection.Table<T>() select i).ToList();
        }

        public T GetEntity<T>(int id) where T : class, IHasID, new()
        {
            return connection.Get<T>(id);
        }

        public void GetChildern<T>(T entity, bool recursive) where T : class, IHasID, new()
        {
            if (entity != null)
            {
                connection.GetChildren<T>(entity, recursive);
            }
        }

        public void SaveEntity<T>(T entity) where T : IHasID
        {
            connection.Insert(entity);
        }

        public void UpdateEntity<T>(T entity) where T : IHasID
        {
            connection.Update(entity);
        }
    }
}
