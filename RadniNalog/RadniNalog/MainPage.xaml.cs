﻿using RadniNalog.Model;
using RadniNalog.Native;
using RadniNalog.ViewModel;
using RadniNalog.ViewModel.WorkOrderViews;
using SQLite;
using SQLite.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace RadniNalog
{
    public partial class MainPage : ContentPage
    {
        private SQLiteRepositoryImpl sqliteRepository = null;

        public MainPage()
        {
            InitializeComponent();
            
            SQLiteConnection sqliteConnection = DependencyService.Get<IConnector>().GetConnection();
            CreateDatabaseTables(sqliteConnection);
            sqliteRepository = new SQLiteRepositoryImpl(sqliteConnection);
        }

        private void CreateDatabaseTables(SQLiteConnection sqliteConnection)
        {
            sqliteConnection.CreateTable<WorkUnit>();
            sqliteConnection.CreateTable<City>();
            sqliteConnection.CreateTable<Address>();
            sqliteConnection.CreateTable<Organization>();
            sqliteConnection.CreateTable<WorkOrder>();
            sqliteConnection.CreateTable<WorkDone>();
        }

        void btn_WorkOrder(object sender, EventArgs args)
        {
            NavigateTo(new WorkOrderPage(sqliteRepository));
        }

        void btn_WorkOrdersServer(object sender, EventArgs args)
        {
            NavigateTo(new ViewWorkOrderServer(sqliteRepository));
        }

        void btn_WorkUnits(object sender, EventArgs args)
        {
            NavigateTo(new WorkUnitPage(sqliteRepository));
        }

        void btn_Organizations(object sender, EventArgs args)
        {
            NavigateTo(new Organizations(sqliteRepository));
        }

        async private void NavigateTo(Page page)
        {
            await Navigation.PushAsync(page);
        }
    }
}
