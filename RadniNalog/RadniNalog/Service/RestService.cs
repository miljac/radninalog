﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace RadniNalog.Service
{
    public class RestService
    {
        private readonly String BASE_URL = "http://rnl-miljac.rhcloud.com/jobs";

        public async Task<bool> PostToServer<T>(T objectT) where T : class, new()
        {
            HttpClient client = new HttpClient();
            String json = JsonConvert.SerializeObject(objectT);
            StringContent jsonContent = new StringContent(json, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync(BASE_URL, jsonContent);
            if (response.IsSuccessStatusCode) return true;
            return false;
        }

        public async Task<T> GetFromServer<T>(String url) where T : class, new()
        {
            HttpClient client = new HttpClient();
            Uri uri = new Uri(BASE_URL + url);
            HttpResponseMessage response = await client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                String jsonContent = await response.Content.ReadAsStringAsync();
                T objectT = (T)JsonConvert.DeserializeObject(jsonContent);
                return objectT;
            }            
            return null;
        }

        public async Task<List<T>> GetAllFromServer<T>() where T : class, new()
        {
            HttpClient client = new HttpClient();
            HttpResponseMessage response = await client.GetAsync(BASE_URL);
            if (response.IsSuccessStatusCode)
            {
                String jsonContent = await response.Content.ReadAsStringAsync();
                List<T> objectT = JsonConvert.DeserializeObject<List<T>>(jsonContent);
                return objectT;
            }
            return null;
        }
    }
}
